# README

This project is to fulfill the requirements given for the software development homework.

### Installation Steps

- open command prompt or terminal. cd where you want to clone the project.
- clone project using ````git clone https://seth-e-white@bitbucket.org/seth-e-white/code-project-seth.git````
- cd to ````code-project-seth````
- run ````npm install```` from root directory.
- run ````npm run install-all```` from root directory. This installs modules for client & server
- run ````npm run start-all```` from root directory. This will start the server and client.
- Client page should auto load in browser. If not go to browser and navigate to localhost:3000
- Server is running on port 8000! if server can't start - check to see if anything is already running on these ports.

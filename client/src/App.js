import "./App.css";
import { useEffect, useState } from "react";
import axios from "axios";

//Components
import { Thumbnails } from "./components/thumbnails";
import { LargeContent } from "./components/largeContent";
import { Header } from "./components/header";
import { Footer } from "./components/footer";

//Utility
import { Constants } from "./utility/constants";

function App() {
  const [imageData, setImageData] = useState([]);
  const [pageInfo, setPageInfo] = useState({
    pageNumber: 1,
    numberOfPages: 1,
    selectedImage: {},
  });

  const getImageData = (pageNumber) => {
    axios
      .get(`http://localhost:8000/api/images?page=${pageNumber}`)
      .then((response) => {
        setImageData(response.data);
        console.log(response.data);
        setPageInfo((prevState) => ({
          ...prevState,
          numberOfPages: response.data.numberOfPages,
          selectedImage: response.data.data[0] ? response.data.data[0] : {},
        }));
      });
  };

  useEffect(() => getImageData(pageInfo.pageNumber), [pageInfo.pageNumber]);

  return (
    <div id="container">
      {imageData ? (
        <>
          <Header headerName={Constants.header.main} />
          <div id="main" role="main">
            <LargeContent pageInfo={pageInfo} />
            <Thumbnails
              setPageInfo={setPageInfo}
              pageInfo={pageInfo}
              imageData={imageData}
            />
          </div>
          <Footer />
        </>
      ) : (
        <span>Loading...</span>
      )}
    </div>
  );
}

export default App;

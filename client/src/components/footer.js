import { Constants } from "../utility/constants";

export const Footer = () => {
  return (
    <footer>
      <a href="instructions.pdf">{Constants.download.pdf}</a>
    </footer>
  );
};

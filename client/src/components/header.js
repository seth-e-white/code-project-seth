export const Header = ({ headerName }) => {
  return <header>{headerName}</header>;
};

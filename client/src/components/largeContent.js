import { LargeContentDetails } from "./largeContentDetails";

export const LargeContent = ({ pageInfo }) => {
  const { selectedImage } = pageInfo;

  return (
    <div id="large">
      <div className="group">
        <img
          src={`images/large/${selectedImage.image}`}
          alt={selectedImage.image}
          width="430"
          height="360"
        />
        <LargeContentDetails selectedImage={selectedImage} />
      </div>
    </div>
  );
};

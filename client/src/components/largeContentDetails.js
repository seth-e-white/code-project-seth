export const LargeContentDetails = ({ selectedImage }) => {
  return (
    <div className="details">
      <p>
        <strong>Title</strong> {selectedImage.title}
      </p>
      <p>
        <strong>Description</strong> {selectedImage.description}
      </p>
      <p>
        <strong>Cost</strong> ${selectedImage.cost}
      </p>
      <p>
        <strong>ID #</strong> {selectedImage.id}
      </p>
      <p>
        <strong>Thumbnail File</strong> {selectedImage.thumbnail}
      </p>
      <p>
        <strong>Large Image File</strong> {selectedImage.image}
      </p>
    </div>
  );
};

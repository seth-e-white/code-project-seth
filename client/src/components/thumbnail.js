export const Thumbnail = ({ isSelected, setPageInfo, image }) => {
  return (
    <>
      <a
        href="#"
        className={isSelected ? "active" : ""}
        title={image.thumbnail}
        onClick={() =>
          setPageInfo((prevState) => ({
            ...prevState,
            selectedImage: image,
          }))
        }
      >
        <img
          src={`images/thumbnails/${image.thumbnail}`}
          alt={image.thumbnail}
          width="145"
          height="121"
        />
        <span>{image.id}</span>
      </a>
    </>
  );
};

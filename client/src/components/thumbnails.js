import { Thumbnail } from "./thumbnail";

export const Thumbnails = ({ pageInfo, setPageInfo, imageData }) => {
  const data = imageData.data ? imageData.data : [];

  return (
    <div className="thumbnails">
      <div className="group">
        {data.length > 0 &&
          data.map((image) => {
            return (
              <Thumbnail
                setPageInfo={setPageInfo}
                isSelected={pageInfo.selectedImage.id === image.id}
                key={image.id}
                image={image}
              />
            );
          })}
        <span
          className={`previous ${pageInfo.pageNumber === 1 ? "disabled" : ""}`}
          title="Previous"
          disabled
          onClick={() =>
            setPageInfo((prevState) => ({
              ...prevState,
              pageNumber: pageInfo.pageNumber - 1,
            }))
          }
        >
          Previous
        </span>
        <span
          className={`next ${
            pageInfo.pageNumber === pageInfo.numberOfPages ? "disabled" : ""
          }`}
          title="Next"
          disabled={
            pageInfo.pageNumber === pageInfo.numberOfPages ? true : false
          }
          onClick={() =>
            setPageInfo((prevState) => ({
              ...prevState,
              pageNumber: pageInfo.pageNumber + 1,
            }))
          }
        >
          Next
        </span>
      </div>
    </div>
  );
};

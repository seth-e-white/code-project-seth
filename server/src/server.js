const express = require("express");
const app = express();
const port = 8000;

const cors = require("cors");
app.use(
  cors({
    origin: "http://localhost:3000",
  })
);

let imageData = require("../data/templates.json");

app.get("/api/images", function (req, res) {
  let pageNumber = req.query.page;

  let totalImages = imageData.length;
  let pageSize = 4;
  let numberOfPages = Math.ceil(totalImages / pageSize);
  let data = imageData.slice(
    (pageNumber - 1) * pageSize,
    pageNumber * pageSize
  );

  const response = {
    totalImages: totalImages,
    pageSize: pageSize,
    numberOfPages: numberOfPages,
    data: data,
  };

  res.send(response);
});

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});
